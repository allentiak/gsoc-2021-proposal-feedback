Format: 3.0 (quilt)
Source: bidi-clojure
Binary: libbidi-clojure
Architecture: all
Version: 2.1.6-1
Maintainer: Debian Clojure Maintainers <pkg-clojure-maintainers@lists.alioth.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Homepage: https://github.com/juxt/bidi
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/clojure-team/bidi-clojure
Vcs-Git: https://salsa.debian.org/clojure-team/bidi-clojure.git
Testsuite: autopkgtest
Testsuite-Triggers: clojure, libring-mock-clojure
Build-Depends: debhelper-compat (= 13), default-jdk-headless, javahelper, maven-repo-helper, libclojure-java, libprismatic-schema-clojure (>= 1.1.12), libring-core-clojure (>= 1.6.2-4), libring-mock-clojure, libtools-reader-clojure (>= 1.3.4), libcompojure-clojure, libnrepl-clojure <!nocheck>, libcomplete-clojure <!nocheck>, leiningen
Package-List:
 libbidi-clojure deb java optional arch=all
Checksums-Sha1:
 3b718ca9074ad42d3852af9a150e36c452cedf3d 28801 bidi-clojure_2.1.6.orig.tar.gz
 ba0a14b4ed655d54c4639106b8561d34ea7329d6 3864 bidi-clojure_2.1.6-1.debian.tar.xz
Checksums-Sha256:
 3d34bd048263ba9fcf19eb2eb5bc3cd5c6f61a15fc3f043079a1ac062c348ed3 28801 bidi-clojure_2.1.6.orig.tar.gz
 196d369c04137bf020df0a691c39af1be8c3b4433a3367432d613efb54bfd078 3864 bidi-clojure_2.1.6-1.debian.tar.xz
Files:
 ba8d5f4f07f3bd3de3382c046bd83fe4 28801 bidi-clojure_2.1.6.orig.tar.gz
 f45c86550ced1c73854a2cfc5752ac8e 3864 bidi-clojure_2.1.6-1.debian.tar.xz
